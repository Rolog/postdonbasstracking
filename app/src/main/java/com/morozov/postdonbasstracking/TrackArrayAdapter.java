package com.morozov.postdonbasstracking;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.RequiresApi;

import org.ocpsoft.prettytime.PrettyTime;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TrackArrayAdapter extends ArrayAdapter<Track> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView lastCheck;
        TextView trackNum;
        TextView lastUpdate;
        TextView currentStatus;
        TextView name;
        ImageView imageView;
    }

    // Кэш для уже загруженных объектов Bitmap
    private Map<String, Bitmap> bitmaps = new HashMap<>();

    // Конструктор для инициализации унаследованных членов суперкласса
    public TrackArrayAdapter(Context context, List<Track> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Track track = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            //viewHolder.conditionImageView = (ImageView) convertView.findViewById(R.id.conditionImageView);
            viewHolder.lastCheck = (TextView) convertView.findViewById(R.id.lastCheck);
            viewHolder.trackNum = (TextView) convertView.findViewById(R.id.trackNum);
            viewHolder.lastUpdate = (TextView) convertView.findViewById(R.id.lastUpdate);
            viewHolder.currentStatus = (TextView) convertView.findViewById(R.id.currentStatus);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Если значок погодных условий уже загружен, использовать его;
        // в противном случае загрузить в отдельном потоке
        /*if (bitmaps.containsKey(day.iconURL)) {
            viewHolder.conditionImageView.setImageBitmap(bitmaps.get(day.iconURL));
        }
        else {
            // Загрузить и вывести значок погодных условий
            new LoadImageTask(viewHolder.conditionImageView).execute(
                    day.iconURL);
        }*/

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        PrettyTime p = new PrettyTime(new Locale("ru"));
        try {
            viewHolder.lastCheck.setText(track.lastCheck + "  (" + p.format(new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(track.lastCheck)) + ")");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.trackNum.setText(track.trackNum);
        viewHolder.lastUpdate.setText(track.lastUpdate);
        viewHolder.currentStatus.setText(track.currentStatus);
        viewHolder.name.setText(track.name);

        //Drawable background = viewHolder.imageView.getBackground();

        if (track.success.equals(TrackStatus.NOT_PROGRESS))
            viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_red));
        else if (track.success.equals(TrackStatus.IN_MOVIE))
            viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_yellow));
        else if (track.success.equals(TrackStatus.ARRIVED))
            viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_green));
        else if (track.success.equals(TrackStatus.SUCCESS))
            viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_green_check));

        return convertView; // Вернуть готовое представление элемента
    }

    // AsyncTask для загрузки изображения в отдельном потоке
    private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imageView; // Для вывода миниатюры

        // Сохранение ImageView для загруженного объекта Bitmap
        public LoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        // загрузить изображение; params[0] содержит URL-адрес изображения
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            HttpURLConnection connection = null;

            try {
                URL url = new URL(params[0]); // Создать URL для изображения

                // Открыть объект HttpURLConnection, получить InputStream
                // и загрузить изображение
                connection = (HttpURLConnection) url.openConnection();

                try (InputStream inputStream = connection.getInputStream()) {
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    bitmaps.put(params[0], bitmap); // Кэширование
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                connection.disconnect(); // Закрыть HttpURLConnection
                }

            return bitmap;
        }

        // Связать значок погодных условий с элементом списка
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
