package com.morozov.postdonbasstracking;

import static com.morozov.postdonbasstracking.MainActivity.thisStatic;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.morozov.postdonbasstracking.data.DatabaseDescription;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int TRACK_LOADER = 0; // identifies the Loader
    private TextView lastCheckTextView;
    private Uri tracktUri; // Uri of selected contact
    private TextView nameTextView; // displays contact's name
    private TextView trackNumTextView; // displays contact's phone
    private TextView lastUpdateTextView; // displays contact's email
    private TextView currentStatusTextView; // displays contact's street
    private TextView successTextView; // displays contact's city
    private WebView html;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        try {
            Bundle b = getIntent().getExtras();
            tracktUri = (Uri) b.get(MainActivity.TRACK_URI);
        } catch (Exception ignored) {}

        // get the EditTexts
        lastCheckTextView = (TextView) findViewById(R.id.lastCheck);
        nameTextView = (TextView) findViewById(R.id.name);
        trackNumTextView = (TextView) findViewById(R.id.trackNum);
        lastUpdateTextView = (TextView) findViewById(R.id.lastUpdate);
        currentStatusTextView = (TextView) findViewById(R.id.currentStatus);
        successTextView = (TextView) findViewById(R.id.success);
        html = (WebView) findViewById(R.id.html);

        // load the contact
        getSupportLoaderManager().initLoader(TRACK_LOADER, null, this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.fragment_details_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteTrack();
                return true;
            case R.id.action_update:
                updateTrack();
                return true;
            case R.id.action_rename:
                renameTrack();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // delete a contact
    private void deleteTrack() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this);

        builder.setTitle("Вы уверены?");
        builder.setMessage("Данный трек-номер будет удален.");

        // provide an OK button that simply dismisses the dialog
        builder.setPositiveButton("Удалить",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog, int button) {

                        // use Activity's ContentResolver to invoke
                        // delete on the AddressBookContentProvider
                        thisStatic.getContentResolver().delete(tracktUri, null, null);
                        //MainActivity.loadListTracks(thisStatic);
                        //MainActivity.trackArrayAdapter.notifyDataSetChanged();
                        onBackPressed();
                    }
                }
        );

        builder.setNegativeButton("Отмена", null);
        builder.create(); // return the AlertDialog
        builder.show();
    }

    private void updateTrack() {
        if (MainActivity.isInternetConnect(MainActivity.thisStatic)) {
            MainActivity.GetTrackTask getTrackTask = new MainActivity.GetTrackTask(MainActivity.thisStatic);
            try {
                MainActivity.TaskObject taskObject = getTrackTask.execute(new MainActivity.TaskObject(nameTextView.getText().toString(),
                                trackNumTextView.getText().toString(), MainActivity.createURL(trackNumTextView.getText().toString(), MainActivity.thisStatic))).get();
                if (taskObject != null) {
                    Toast.makeText(MainActivity.thisStatic, "Обновлено", Toast.LENGTH_LONG).show();
                    reloadFragment();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else
            Toast.makeText(MainActivity.thisStatic, "Нет подключения к интернету", Toast.LENGTH_LONG).show();
    }

    private void reloadFragment() {
        // Reload current fragment
        this.recreate();
    }

    private void renameTrack() {

        LayoutInflater li = LayoutInflater.from(this);
        View renameTrackView = li.inflate(R.layout.rename_track, null);
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(
                this);
        alertDialogBuilder.setView(renameTrackView);

        EditText renameTrack = (EditText) renameTrackView.findViewById(R.id.renameTitle);
        renameTrack.setText(nameTextView.getText().toString());

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Переименовать",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //Изменение в БД
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DatabaseDescription.DBTrack.COLUMN_NAME,
                                        renameTrack.getText().toString());
                                thisStatic.getContentResolver().update(tracktUri, contentValues, null, null);
                                reloadFragment();
                                MainActivity.loadListTracks(thisStatic);
                            }
                        })
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        CursorLoader cursorLoader;

        switch (id) {
            case TRACK_LOADER:
                cursorLoader = new CursorLoader(this,
                        tracktUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        // if the contact exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int nameIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_NAME);
            int trackNumIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_TRACK_NUM);
            int lastUpdateIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_LAST_UPDATE);
            int currentStatusIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_CURRENT_STATUS);
            int successIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_SUCCESS);
            int htmlIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_FULL_HTML_STATUS);
            int lastCheckIndex = data.getColumnIndex(DatabaseDescription.DBTrack.COLUMN_LAST_CHECK);

            // fill TextViews with the retrieved data
            PrettyTime p = new PrettyTime(new Locale("ru"));
            try {
                lastCheckTextView.setText(data.getString(lastCheckIndex) + "  (" + p.format(new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(data.getString(lastCheckIndex))) + ")");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            nameTextView.setText(data.getString(nameIndex));
            trackNumTextView.setText(data.getString(trackNumIndex));
            lastUpdateTextView.setText(data.getString(lastUpdateIndex));
            currentStatusTextView.setText(data.getString(currentStatusIndex));
            successTextView.setText(getLocalizationSuccess(data.getString(successIndex)));

            String content = "<link rel='stylesheet' href='file:///android_asset/detail.css'>" + data.getString(htmlIndex);
            html.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);

            getSupportActionBar().setTitle(trackNumTextView.getText().toString());
        }
    }

    // called by LoaderManager when the Loader is being reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }

    public String getLocalizationSuccess(String success) {
        switch (TrackStatus.valueOf(success)) {
            case SUCCESS:
                return "Выполнено";
            case IN_MOVIE:
                return "В движении";
            case ARRIVED:
                return "В пункте выдачи";
            case NOT_PROGRESS:
                return "Не определен";
            default:
                return "";
        }
    }
}