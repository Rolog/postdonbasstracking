// AddressBookDatabaseHelper.java
// SQLiteOpenHelper subclass that defines the app's database
package com.morozov.postdonbasstracking.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import static com.morozov.postdonbasstracking.data.DatabaseDescription.*;

class PostTrackDatabaseHelper extends SQLiteOpenHelper {
   private static final String DATABASE_NAME = "PostTrack.db";
   private static final int DATABASE_VERSION = 2;

   // constructor
   public PostTrackDatabaseHelper(Context context) {
      super(context, DATABASE_NAME, null, DATABASE_VERSION);
   }

   // creates the contacts table when the database is created
   @Override
   public void onCreate(SQLiteDatabase db) {
      // SQL for creating the contacts table
      final String CREATE_TRACKS_TABLE =
         "CREATE TABLE " + DBTrack.TABLE_NAME + "(" +
                 DBTrack._ID + " integer primary key, " +
                 DBTrack.COLUMN_NAME + " TEXT, " +
                 DBTrack.COLUMN_LAST_UPDATE + " TEXT, " +
                 DBTrack.COLUMN_CURRENT_STATUS + " TEXT, " +
                 DBTrack.COLUMN_FULL_HTML_STATUS + " TEXT, " +
                 DBTrack.COLUMN_SUCCESS + " integer, " +
                 DBTrack.COLUMN_TRACK_NUM + " TEXT, " +
                 DBTrack.COLUMN_LAST_CHECK + " TEXT);";
      db.execSQL(CREATE_TRACKS_TABLE); // create the contacts table
   }

   // normally defines how to upgrade the database when the schema changes
   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      switch(oldVersion) {
         case 1:
            String sql = "ALTER TABLE " + DBTrack.TABLE_NAME + " ADD COLUMN " + DBTrack.COLUMN_LAST_CHECK + " TEXT";
            db.execSQL(sql);
            break;
      }
   }
}
