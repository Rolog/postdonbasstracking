package com.morozov.postdonbasstracking.update;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.morozov.postdonbasstracking.R;

public class Dialogs {
    public static CircularProgressAlertDialog circularProgressDialog(Context context, String title, String msg,
                                                                     boolean cancelable, DialogInterface.OnCancelListener onCancelListener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_progress_dialog_circular, null);
        final TextView message = view.findViewById(R.id.message);
        final CircularProgressIndicator progress = view.findViewById(R.id.progress);
        if (message != null)
            message.setText(msg);
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        if (title != null)
            builder.setTitle(title);
        builder.setView(view);
        builder.setCancelable(cancelable);
        if (onCancelListener != null)
            builder.setOnCancelListener(onCancelListener);
        return new CircularProgressAlertDialog(builder.create(), message, progress);
    }

    public static LinearProgressAlertDialog linearProgressDialog(Context context, String title, String msg,
                                                                 boolean cancelable, DialogInterface.OnCancelListener onCancelListener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_progress_dialog_linear, null);
        final TextView message = view.findViewById(R.id.message);
        final LinearProgressIndicator progress = view.findViewById(R.id.progress);
        if (message != null)
            message.setText(msg);
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        if (title != null)
            builder.setTitle(title);
        builder.setView(view);
        builder.setCancelable(cancelable);
        if (onCancelListener != null)
            builder.setOnCancelListener(onCancelListener);
        return new LinearProgressAlertDialog(builder.create(), message, progress);
    }

    public static class LinearProgressAlertDialog {
        private final AlertDialog alertDialog;
        private final TextView message;
        private final LinearProgressIndicator progressIndicator;

        public LinearProgressAlertDialog(AlertDialog alertDialog,
                                         TextView message, LinearProgressIndicator progressIndicator) {
            this.alertDialog = alertDialog;
            this.message = message;
            this.progressIndicator = progressIndicator;
        }

        public AlertDialog getAlertDialog() {
            return alertDialog;
        }

        public TextView getMessage() {
            return message;
        }

        public LinearProgressIndicator getProgressIndicator() {
            return progressIndicator;
        }
    }

    public static class CircularProgressAlertDialog {
        private final AlertDialog alertDialog;
        private final TextView message;
        private final CircularProgressIndicator progressIndicator;

        public CircularProgressAlertDialog(AlertDialog alertDialog,
                                           TextView message, CircularProgressIndicator progressIndicator) {
            this.alertDialog = alertDialog;
            this.message = message;
            this.progressIndicator = progressIndicator;
        }

        public AlertDialog getAlertDialog() {
            return alertDialog;
        }

        public TextView getMessage() {
            return message;
        }

        public CircularProgressIndicator getProgressIndicator() {
            return progressIndicator;
        }
    }
}

