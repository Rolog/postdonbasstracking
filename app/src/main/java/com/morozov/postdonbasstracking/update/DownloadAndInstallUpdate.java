package com.morozov.postdonbasstracking.update;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.morozov.postdonbasstracking.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadAndInstallUpdate extends AsyncTask<Void, Long, Void> implements DialogInterface.OnCancelListener {
    private static final String LOG_TAG 			= DownloadAndInstallUpdate.class.getName();

    private Context context;
    private final UpdateInfo updateInfo;
    private Dialogs.LinearProgressAlertDialog progressDialog;
    private boolean isRun;
    private String md5;
    private boolean error;

    public DownloadAndInstallUpdate(Context context, UpdateInfo updateInfo) {
        this.context = context;
        this.updateInfo = updateInfo;
        this.isRun = false;
        this.error = false;
    }

    private void initProgressDialog() {
        progressDialog = Dialogs.linearProgressDialog(context,
                context.getString(R.string.update_title), context.getString(R.string.update_download),
                true, this);
        progressDialog.getProgressIndicator().setMax(100);
    }

    public void onCreate(Context context) {
        this.context = context;
        initProgressDialog();
        if (isRun) {
            progressDialog.getAlertDialog().show();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        initProgressDialog();
        progressDialog.getAlertDialog().show();
        isRun = true;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog != null && progressDialog.getAlertDialog().isShowing() && !(((AppCompatActivity)context).isFinishing()))
            progressDialog.getAlertDialog().dismiss();
        isRun = false;
        if (error) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
            builder.setTitle(R.string.update_title);
            builder.setMessage(R.string.update_download_error);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            builder.show();
        } else {
                Uri uri;
                String root = Environment.getExternalStorageDirectory().toString();
                File file = new File(root+"/Download/postdonbasstracking.apk");
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    uri = Uri.fromFile(file);
                else
                    uri = FileProvider.getUriForFile(context,
                            context.getApplicationContext().getPackageName() + ".provider", file);
                Intent apkIntent = new Intent(Intent.ACTION_VIEW);
                apkIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                apkIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                apkIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                context.startActivity(apkIntent);
        }
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        super.onProgressUpdate(values);
        progressDialog.getProgressIndicator().setProgress((int)Math.round((Double.valueOf(values[1]) / values[0]) * 100));
    }

    @Override
    protected Void doInBackground(Void... params) {
        int count;
        try {
            String root = Environment.getExternalStorageDirectory().toString();

            URL url = new URL("https://bitbucket.org/Rolog/postdonbasstracking/downloads/postdonbasstracking.apk");

            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            long lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream());

            // Output stream to write file

            OutputStream output = new FileOutputStream(root+"/Download/postdonbasstracking.apk");
            byte[] data = new byte[1024];

            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;

                // writing data to file
                output.write(data, 0, count);
                publishProgress(lenghtOfFile, total);

            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            return null;
        }
        return null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
    }

}
