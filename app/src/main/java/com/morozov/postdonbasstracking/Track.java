package com.morozov.postdonbasstracking;

import android.os.Build;
import androidx.annotation.RequiresApi;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Track {
    public Long id;
    public String lastCheck;
    public String trackNum;
    public String lastUpdate;
    public String currentStatus;
    public String fullHTMLStatus;
    public String name;
    public TrackStatus success;

    public Track(Date lastCheck, String trackNum, Date lastUpdate, String currentStatus, String fullHTMLStatus, String name, TrackStatus success ) {
        this.trackNum = trackNum;
        SimpleDateFormat DateFor = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        this.lastCheck = DateFor.format(lastCheck);
        this.lastUpdate = DateFor.format(lastUpdate);
        this.currentStatus = currentStatus;
        this.name = name;
        this.fullHTMLStatus = fullHTMLStatus;
        this.success = success;
    }

    public Track(Long id, Date lastCheck, String trackNum, Date lastUpdate, String currentStatus, String fullHTMLStatus, String name, TrackStatus success) {
        this.id = id;
        this.trackNum = trackNum;
        SimpleDateFormat DateFor = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        this.lastCheck = DateFor.format(lastCheck);
        this.lastUpdate = DateFor.format(lastUpdate);
        this.currentStatus = currentStatus;
        this.name = name;
        this.fullHTMLStatus = fullHTMLStatus;
        this.success = success;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastCheck() {
        return lastCheck;
    }

    public void setLastCheck(String lastCheck) {
        this.lastCheck = lastCheck;
    }

    public String getTrackNum() {
        return trackNum;
    }

    public void setTrackNum(String trackNum) {
        this.trackNum = trackNum;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getFullHTMLStatus() {
        return fullHTMLStatus;
    }

    public void setFullHTMLStatus(String fullHTMLStatus) {
        this.fullHTMLStatus = fullHTMLStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrackStatus getSuccess() {
        return success;
    }

    public void setSuccess(TrackStatus success) {
        this.success = success;
    }
}
