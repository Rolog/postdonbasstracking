package com.morozov.postdonbasstracking.jobservice;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;

import java.util.concurrent.TimeUnit;

public class TrackUpdateRequestsReceiver extends BroadcastReceiver {
    //private static final String TAG = TrackUpdateRequestsReceiver.class.getSimpleName();

    public static final String ACTION_TRACK_UPDATE = "ACTION_TRACK_UPDATE";

    private static int sJobId = 1;

    private static int updateMinutes = 180;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onReceive(Context context, Intent intent) {

        //Log.d(TAG, "onReceive: action: " + intent.getAction());

        switch (intent.getAction()) {
            case ACTION_TRACK_UPDATE:
                scheduleJob(context);
                break;
            default:
                throw new IllegalArgumentException("Unknown action.");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob(Context context) {
        ComponentName jobService = new ComponentName(context, TrackUpdateJobService.class);
        JobInfo.Builder exerciseJobBuilder = new JobInfo.Builder(sJobId, jobService);
        //exerciseJobBuilder.setMinimumLatency(TimeUnit.MINUTES.toMillis(updateMinutes));
        //exerciseJobBuilder.setOverrideDeadline(TimeUnit.MINUTES.toMillis(updateMinutes));
        exerciseJobBuilder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        exerciseJobBuilder.setPersisted(true);
        //exerciseJobBuilder.setBackoffCriteria(TimeUnit.SECONDS.toMillis(10), JobInfo.BACKOFF_POLICY_LINEAR);
        //exerciseJobBuilder.setPeriodic(1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            exerciseJobBuilder.setMinimumLatency(TimeUnit.MINUTES.toMillis(updateMinutes));//TimeUnit.MINUTES.toMillis(updateMinutes));
        else
            exerciseJobBuilder.setPeriodic(TimeUnit.MINUTES.toMillis(updateMinutes));
        //exerciseJobBuilder.setPeriodic(TimeUnit.MINUTES.toMillis(20));
        //exerciseJobBuilder.setBackoffCriteria(TimeUnit.MINUTES.toMillis(updateMinutes), JobInfo.BACKOFF_POLICY_LINEAR);

        //Log.i(TAG, "scheduleJob: adding job to scheduler");

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(exerciseJobBuilder.build());
    }
}
