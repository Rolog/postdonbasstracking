package com.morozov.postdonbasstracking.jobservice;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import com.morozov.postdonbasstracking.MainActivity;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class TrackUpdateJobService extends JobService {
    private static final String TAG = TrackUpdateJobService.class.getSimpleName();

    public TrackUpdateJobService() {
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        //Log.i(TAG, "onStartJob: starting job with id: " + params.getJobId());

        TrackUpdateIntentService.startActionWriteExercise(getApplicationContext());
        jobFinished(params, true);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        //Log.i(TAG, "onStopJob: stopping job with id: " + params.getJobId());
        return false;
    }

}

