package com.morozov.postdonbasstracking.jobservice;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.morozov.postdonbasstracking.MainActivity;
import com.morozov.postdonbasstracking.TrackStatus;
import com.morozov.postdonbasstracking.UpdateTrackStatus;
import com.morozov.postdonbasstracking.data.DatabaseDescription;

import java.util.HashMap;

public class TimeNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (MainActivity.isInternetConnect(context.getApplicationContext())) {
            Cursor cursor = context.getApplicationContext().getContentResolver().query(
                    DatabaseDescription.DBTrack.CONTENT_URI, null, null, null, null);

            String COLUMN_TRACK_NUM;
            String COLUMN_NAME;
            TrackStatus COLUMN_SUCCESS;

            MainActivity.updateObject = new MainActivity.UpdateObject();
            MainActivity.updateObject.setCountInitTracks(cursor.getCount());
            HashMap<String, UpdateTrackStatus> infoObject = new HashMap<>(MainActivity.updateObject.getInfoObject());
            while (cursor.moveToNext()) {

                COLUMN_TRACK_NUM = cursor.getString(cursor
                        .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_TRACK_NUM));
                COLUMN_NAME = cursor.getString(cursor
                        .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_NAME));
                COLUMN_SUCCESS = TrackStatus.valueOf(cursor.getString(cursor
                        .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_SUCCESS)));
                if (COLUMN_SUCCESS != TrackStatus.SUCCESS) {
                    infoObject.put(COLUMN_TRACK_NUM, UpdateTrackStatus.NO_VALUE);
                    MainActivity.updateObject.setInfoObject(infoObject);
                    MainActivity.GetTrackTask getTrackTask = new MainActivity.GetTrackTask(context.getApplicationContext());
                    getTrackTask.execute(new MainActivity.TaskObject(COLUMN_NAME,
                            COLUMN_TRACK_NUM, MainActivity.createURL(COLUMN_TRACK_NUM, context.getApplicationContext())));
                } else {
                    infoObject.put(COLUMN_TRACK_NUM, UpdateTrackStatus.NO_CHANGES);
                    MainActivity.updateObject.setInfoObject(infoObject);
                }
            }
            cursor.close();
        }

        // Установим следующее напоминание.
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (AlarmManager.INTERVAL_HOUR)/*AlarmManager.INTERVAL_DAY*/, pendingIntent);
    }
}
