package com.morozov.postdonbasstracking.jobservice;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import com.morozov.postdonbasstracking.MainActivity;
import com.morozov.postdonbasstracking.Track;
import com.morozov.postdonbasstracking.TrackStatus;
import com.morozov.postdonbasstracking.data.DatabaseDescription;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TrackUpdateIntentService extends IntentService {
    private static final String ACTION_UPDATE_TRACK_STATUS = "com.morozov.postdonbasstracking.jobservice.ACTION_UPDATE_TRACK_STATUS";

    public TrackUpdateIntentService() {
        super("TrackUpdateIntentService");
    }

    public static void startActionWriteExercise(Context context) {
        Intent intent = new Intent(context, TrackUpdateIntentService.class);
        intent.setAction(ACTION_UPDATE_TRACK_STATUS);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_TRACK_STATUS.equals(action)) {
                handleActionWriteExercise();
            }
        }
    }

    private void handleActionWriteExercise() {
        Cursor cursor = getApplicationContext().getContentResolver().query(
                DatabaseDescription.DBTrack.CONTENT_URI, null, null, null, null);

        String COLUMN_TRACK_NUM;
        String COLUMN_NAME;
        TrackStatus COLUMN_SUCCESS;

        while (cursor.moveToNext()) {

            COLUMN_TRACK_NUM = cursor.getString(cursor
                    .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_TRACK_NUM));
            COLUMN_NAME = cursor.getString(cursor
                    .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_NAME));
            COLUMN_SUCCESS = TrackStatus.valueOf(cursor.getString(cursor
                    .getColumnIndex(DatabaseDescription.DBTrack.COLUMN_SUCCESS)));
            if (COLUMN_SUCCESS != TrackStatus.SUCCESS) {
                MainActivity.GetTrackTask getTrackTask = new MainActivity.GetTrackTask(getApplicationContext());
                getTrackTask.execute(new MainActivity.TaskObject(COLUMN_NAME,
                        COLUMN_TRACK_NUM, MainActivity.createURL(COLUMN_TRACK_NUM, getApplicationContext())));
            }
        }
        cursor.close();

        /*try {
            FileWriter fileWriter = new FileWriter(getFilesDir().getPath() + "exercise.txt");
            fileWriter.write("Exercise");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
