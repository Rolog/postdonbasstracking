package com.morozov.postdonbasstracking;

public enum TrackStatus {
    NOT_PROGRESS,
    IN_MOVIE,
    ARRIVED,
    SUCCESS
}
