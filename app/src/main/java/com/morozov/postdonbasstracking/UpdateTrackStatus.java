package com.morozov.postdonbasstracking;

public enum UpdateTrackStatus {
    NEW_VALUE,
    ERROR,
    NO_CHANGES,
    NO_VALUE
}
