package com.morozov.postdonbasstracking;

import static com.morozov.postdonbasstracking.data.DatabaseDescription.DBTrack;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.morozov.postdonbasstracking.jobservice.TimeNotification;
import com.morozov.postdonbasstracking.update.CheckForUpdate;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends AppCompatActivity {
    // Список объектов Weather, представляющих прогноз погоды
    private static List<Track> tracksList = new ArrayList<>();

    // ArrayAdapter связывает объекты Track с элементами ListView
    public static TrackArrayAdapter trackArrayAdapter;
    public static ListView tracksListView; // Для вывода информации

    public final Context context = this;
    public static MainActivity thisStatic;
    private EditText addTrack;
    private EditText addTitle;

    public static final String TRACK_URI = "track_uri";

    public static FloatingActionButton fab;
    public static SwipeRefreshLayout mSwipeRefreshLayout;
    static boolean active = false;
    public static UpdateObject updateObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle(R.string.my_tracks);
        MainActivity.thisStatic = this;

        bypassSSLValidation();

        String[] permissions = checkPermission();
        if (permissions.length > 0) {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    123);
        } else {

            //installtion permission

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!getPackageManager().canRequestPackageInstalls()) {
                    startActivity(new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES).setData(Uri.parse(String.format("package:%s", getPackageName()))));
                } else {
                }
            }

            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            active = true;
            updateObject = new UpdateObject();

            mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                /*String currentDateTime = DateFormat.getDateTimeInstance().format(new Date());
                mList.add(currentDateTime);
                mListView.invalidateViews();*/
                    if (!isInternetConnect(context)) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        return;
                    }
                    if (tracksList.size() == 0) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        return;
                    }
                    updateTracks();
                    //mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            // ArrayAdapter для связывания tracksList с tracksListView
            tracksListView = (ListView) findViewById(R.id.tracksListView);
            trackArrayAdapter = new TrackArrayAdapter(this, tracksList);
            tracksListView.setAdapter(trackArrayAdapter);

            fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isInternetConnect(context)) {
                        return;
                    }
                    LayoutInflater li = LayoutInflater.from(context);
                    View addTrackView = li.inflate(R.layout.add_track, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);
                    alertDialogBuilder.setView(addTrackView);

                    addTrack = (EditText) addTrackView.findViewById(R.id.addTrack);
                    addTitle = (EditText) addTrackView.findViewById(R.id.addTitle);

                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("Добавить",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // get user input and set it to result
                                            // edit text
                                            //result.setText(userInput.getText());

                                            Cursor cursor = context.getContentResolver().query(
                                                    DBTrack.CONTENT_URI, null, DBTrack.COLUMN_TRACK_NUM + "='" + addTrack.getText().toString() + "'",
                                                    null, null);
                                            if (cursor.getCount() > 0) {
                                                Snackbar.make(view, "Такой трек-номер уже отслеживается.", Snackbar.LENGTH_LONG)
                                                        .setAction("Action", null).show();
                                            } else {
                                                GetTrackTask getTrackTask = new GetTrackTask(context);
                                                getTrackTask.execute(new TaskObject(addTitle.getText().toString(),
                                                        addTrack.getText().toString(), createURL(addTrack.getText().toString(), context)));
                                            }
                                        }
                                    })
                            .setNegativeButton("Отмена",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });

            tracksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    displayTrack(DBTrack.buildContactUri(((Track) parent.getItemAtPosition(position)).id), R.id.fragmentContainer);
                }
            });

            //loadListTracks(context);

            //Intent demoIntentForBroadcast =
            //        new Intent(context, TrackUpdateRequestsReceiver.class);

            //demoIntentForBroadcast
            //        .setAction(TrackUpdateRequestsReceiver.ACTION_TRACK_UPDATE);

            //context.sendBroadcast(demoIntentForBroadcast);

            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, TimeNotification.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            // На случай, если мы ранее запускали активити, а потом поменяли время,
            // откажемся от уведомления
            am.cancel(pendingIntent);
            // Устанавливаем разовое напоминание
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);

            CheckForUpdate checkForUpdate = new CheckForUpdate(this, false, false);
            checkForUpdate.execute();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        if (checkPermission().length == 0)
            loadListTracks(context);
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    public void updateTracks() {
        updateObject = new UpdateObject();
        updateObject.countInitTracks = tracksList.size();
        for (Track track : tracksList) {
            if (track.success != TrackStatus.SUCCESS) {
                updateObject.infoObject.put(track.trackNum, UpdateTrackStatus.NO_VALUE);
                GetTrackTask getTrackTask = new GetTrackTask(context);
                getTrackTask.execute(new TaskObject(track.name, track.trackNum, createURL(track.trackNum, context)));
            }
            else {
                updateObject.infoObject.put(track.trackNum, UpdateTrackStatus.NO_CHANGES);
                finishUpdateTasks(context);
            }
        }
        /*while (!updateObject.updateSuccess) {
            updateObject.recalcUpdate();
        }
        Snackbar.make(getWindow().getDecorView().getRootView(), updateObject.countNewValues + " новых статуса(-ов)", 3000)
                .show();*/
        /*loadListTracks(context);*/
    }

    public static void finishUpdateTasks(Context mContext) {
        updateObject.recalcUpdate();
        if (updateObject.updateSuccess) {
            mSwipeRefreshLayout.setRefreshing(false);
            updateObject = new UpdateObject();
            Snackbar.make(getApp().getWindow().getDecorView().getRootView(),
                    updateObject.countNewValues + " новых статуса(-ов), " + updateObject.countErrorValues + " ошибок",
                    3000)
                    .show();
        }
    }

    private void displayTrack(Uri contactUri, int viewID) {
        /*
        DetailFragment detailFragment = new DetailFragment();

        // specify contact's Uri as an argument to the DetailFragment
        Bundle arguments = new Bundle();
        arguments.putParcelable(TRACK_URI, contactUri);
        detailFragment.setArguments(arguments);

        // use a FragmentTransaction to display the DetailFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes DetailFragment to display

         */
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(TRACK_URI, contactUri);
        intent.putExtra("id", 1);
        startActivity(intent);
    }

    public static void loadListTracks (Context mContext) {
        if (active) {
            Cursor cursor = mContext.getContentResolver().query(
                    DBTrack.CONTENT_URI, null, null, null, null);

            String COLUMN_TRACK_NUM;
            String COLUMN_LAST_UPDATE;
            String COLUMN_LAST_CHECK;
            String COLUMN_CURRENT_STATUS;
            String COLUMN_FULL_HTML_STATUS;
            String COLUMN_NAME;
            TrackStatus COLUMN_SUCCESS;
            Long COLUMN_ID;

            tracksList.clear();
            while (cursor.moveToNext()) {

                COLUMN_TRACK_NUM = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_TRACK_NUM));
                COLUMN_LAST_UPDATE = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_LAST_UPDATE));
                COLUMN_LAST_CHECK = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_LAST_CHECK));
                COLUMN_CURRENT_STATUS = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_CURRENT_STATUS));
                COLUMN_FULL_HTML_STATUS = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_FULL_HTML_STATUS));
                COLUMN_NAME = cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_NAME));
                COLUMN_SUCCESS = TrackStatus.valueOf(cursor.getString(cursor
                        .getColumnIndex(DBTrack.COLUMN_SUCCESS)));
                COLUMN_ID = cursor.getLong(cursor.getColumnIndex(DBTrack._ID));
                try {
                    tracksList.add(new Track(COLUMN_ID,
                            COLUMN_LAST_CHECK != null ?
                            new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(COLUMN_LAST_CHECK) : new Date(),
                            COLUMN_TRACK_NUM,
                            COLUMN_LAST_UPDATE != null ?
                            new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(COLUMN_LAST_UPDATE) : new Date(),
                            COLUMN_CURRENT_STATUS, COLUMN_FULL_HTML_STATUS,
                            COLUMN_NAME, COLUMN_SUCCESS));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
            trackArrayAdapter.notifyDataSetChanged();

            cursor.close();
            getApp().setTitle(mContext.getResources().getString(R.string.my_tracks) + "  (" + tracksList.size() + ")");
        }

    }

    public static MainActivity getApp() {
        return MainActivity.thisStatic;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_version);
        try {
            item.setTitle(getString(R.string.version) + this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_updateApp) {
            CheckForUpdate checkForUpdate = new CheckForUpdate(this, true, true);
            checkForUpdate.execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Создание URL веб-сервисы
    public static String createURL(String track, Context mContexst) {
        //String body = "tracking_group%5Bsearch_tracking%5D=" + track +
        //        "&form_build_id=form-6xb_x4h83_LfHa8Y1pnoYIHXuYg1mN2wXzta3auzjjI&form_id=tracking_form";
        //Еще один вариант и это "GET" запрос (может даже лучше)
        //https://postdonbass.com/tracking/get_movie?tracking_number=UB382545861HK

        //if (track.equals("123"))
        //    return "http://dota2tstats.com/playlist/1.txt";
        return mContexst.getResources().getString(R.string.web_service_url).replace("{TRACK}", track)
                + "|"
                + mContexst.getResources().getString(R.string.web_service_api_url).replace("{TRACK}", track);
    }

    /**
     * Обход проверки сертификата
     */
    private void bypassSSLValidation() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
        } catch (NoSuchAlgorithmException | KeyManagementException ex ) {
            ex.printStackTrace();
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    // Обращение к REST-совместимому веб-сервису за погодными данными
    // и сохранение данных в локальном файле HTML
    @SuppressLint("StaticFieldLeak")
    public static class GetTrackTask
            extends AsyncTask<TaskObject, Void, TaskObject> {

        private Context mContext;

        public GetTrackTask (Context context){
            mContext = context;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected TaskObject doInBackground(TaskObject... params) {
            String[] urls = params[0].param.split("\\|");

            HttpsURLConnection connection = null;
            HttpsURLConnection apiConnection = null;

            try {
                URL url = new URL(urls[0]);
                connection = (HttpsURLConnection) url.openConnection();

                if (connection.getResponseCode() != HttpsURLConnection.HTTP_OK)
                    return null;

                String text = getResponseText(connection);
                String csrf = getCsrfToken(text);
                String cookies = getCookies(connection);

                if (csrf == null)
                    return new TaskObject(params[0].name, params[0].num, text);

                URL apiUrl = new URL(urls[1].replace("{CSRF}", csrf));

                apiConnection = (HttpsURLConnection)apiUrl.openConnection();
                apiConnection.setRequestProperty("Cookie", cookies);

                if (apiConnection.getResponseCode() != HttpsURLConnection.HTTP_OK)
                    return null;

                String apiText = getResponseText(apiConnection);

                return new TaskObject(params[0].name, params[0].num, apiText);
            } catch (Exception e ) {
                e.printStackTrace();
            }
            finally {
                if (connection != null)
                    connection.disconnect(); // Закрыть HttpURLConnection

                if (apiConnection != null)
                    apiConnection.disconnect();
            }

            return null;
        }

        private String getCookies(HttpsURLConnection connection) {
            StringBuilder result = new StringBuilder();

            Map<String, List<String>> headers = connection.getHeaderFields();
            if (headers.containsKey("Set-Cookie")) {
                for (String cookie : headers.get("Set-Cookie") ) {
                    result.append(cookie.split(";", 2)[0]);
                    result.append(";");
                }
            }

            return result.toString();
        }

        private String getCsrfToken(String html) {
            Matcher matcher = Pattern.compile("\"csrfToken\":\\s*\"([^\"]+)\"").matcher(html);

            if (matcher.find() && matcher.groupCount() > 0) {
                return matcher.group(1);
            }

            return null;
        }

        private static String getResponseText(HttpsURLConnection connection) {
            StringBuilder builder = new StringBuilder();

            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()))) {

                String line;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            return builder.toString();
        }

        // Обработка ответа JSON и обновление ListView
        @Override
        protected void onPostExecute(TaskObject result) {
            if (result != null) {
                Track track;
                if (result.param != null && !result.param.equals("")) {
                    MainActivity.Status status = getTrackStatus(result.param);
                    track = new Track(new Date(), result.num, status.date, status.status, status.html, result.name, status.success);
                } else {
                    track  = new Track(new Date(), result.num, new Date(), "", "", result.name, TrackStatus.NOT_PROGRESS);
                }

                //Добавление в БД
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBTrack.COLUMN_LAST_CHECK,
                        track.lastCheck);
                contentValues.put(DBTrack.COLUMN_NAME,
                        track.name);
                contentValues.put(DBTrack.COLUMN_CURRENT_STATUS,
                        track.currentStatus);
                contentValues.put(DBTrack.COLUMN_FULL_HTML_STATUS,
                        track.fullHTMLStatus);
                contentValues.put(DBTrack.COLUMN_LAST_UPDATE,
                        track.lastUpdate);
                contentValues.put(DBTrack.COLUMN_SUCCESS,
                        track.success.toString());
                contentValues.put(DBTrack.COLUMN_TRACK_NUM,
                        track.trackNum);
                Cursor cursor = mContext.getContentResolver().query(
                        DBTrack.CONTENT_URI, null, DBTrack.COLUMN_TRACK_NUM + "='" + track.trackNum + "'",
                        null, null);
                if (cursor != null && cursor.getCount() > 0) {
                    if (result.param != null && !result.param.equals("")) {
                        cursor.moveToFirst();
                        TrackStatus oldTrackSuccess = TrackStatus.valueOf(cursor.getString(cursor.getColumnIndex(DBTrack.COLUMN_SUCCESS)));
                        mContext.getContentResolver().update(DBTrack.buildContactUri(cursor.getLong(cursor.getColumnIndex(DBTrack._ID))), contentValues, null, null);
                        if (!track.success.equals(oldTrackSuccess) || (track.success.equals(TrackStatus.IN_MOVIE) && !track.fullHTMLStatus.equals(cursor.getString(cursor.getColumnIndex(DBTrack.COLUMN_FULL_HTML_STATUS))))) {
                            updateObject.infoObject.put(track.trackNum, UpdateTrackStatus.NEW_VALUE);
                            if (!active)
                                pushNotification(track.name, track.currentStatus + " " + track.lastUpdate);
                        }
                        else  {
                            updateObject.infoObject.put(track.trackNum, UpdateTrackStatus.NO_CHANGES);
                        }
                    }
                    else {
                        updateObject.infoObject.put(track.trackNum, UpdateTrackStatus.ERROR);
                    }
                } else {
                    Uri newContactUri = mContext.getContentResolver().insert(
                            DBTrack.CONTENT_URI, contentValues);
                }
                if (active) {
                    loadListTracks(mContext);
                    finishUpdateTasks(mContext);
                }
          /*convertJSONtoArrayList(weather); // Заполнение weatherList
            weatherArrayAdapter.notifyDataSetChanged(); // Связать с ListView
            weatherListView.smoothScrollToPosition(0); // Прокрутить до верха*/
            }
        }

        @SuppressLint("SimpleDateFormat")
        public MainActivity.Status getTrackStatus(String html) {
            MainActivity.Status status = new MainActivity.Status();

            status.html = html.replaceAll("class=\"status_date\">\\s*(\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2})\\s*", "class=\"status_date\">$1</span><span class=\"status-location\">");
            status.success = TrackStatus.NOT_PROGRESS;

            try {
                NodeList nodes = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder()
                        .parse(new InputSource(new StringReader("<div>" + html.replace("&", "") + "</div>")))
                        .getElementsByTagName("div");;

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);

                    if (hasClass(node, "items-status")) {
                        Node statusNode = getChildWithClass(node, "status_bold");
                        Node dateNode = getChildWithClass(node, "status_date");

                        if (statusNode != null && dateNode != null) {
                            status.status = strip(statusNode.getTextContent(), " .!");
                            status.date = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(dateNode.getTextContent().trim());

                            if (html.contains("danger") || html.contains("Отсутствие адресата")) {
                                status.success = TrackStatus.NOT_PROGRESS;
                            } else if (html.contains("Вручено")) {
                                status.success = TrackStatus.SUCCESS;
                            } else if (
                                    html.contains("Прибыло в место вручения") ||
                                    html.contains("Доставлено в место выдачи")
                            ) {
                                status.success = TrackStatus.ARRIVED;
                            } else {
                                status.success = TrackStatus.IN_MOVIE;
                            }

                            return status;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return status;
        }

        private String strip(String text, String stripChars) {
            String literal = Pattern.quote(stripChars);

            return text.replaceAll("^[" + literal + "]*", "")
                    .replaceAll("[" + literal + "]*$", "");
        }

        private boolean hasClass(Node node, String clazz) {
            return node.hasAttributes() &&
                   node.getAttributes().getNamedItem("class") != null &&
                   node.getAttributes().getNamedItem("class").getNodeValue().contains(clazz);
        }

        private Node getChildWithClass(Node node, String clazz) {
            NodeList nodes = node.getChildNodes();

            for (int i = 0; i < nodes.getLength(); i++) {
                Node child = nodes.item(i);
                if (hasClass(child, clazz)) {
                    return child;
                }
            }

            return null;
        }

        public void pushNotification(String name, String text) {
            Intent intentTL = new Intent(mContext, MainActivity.class);
            intentTL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(mContext, 0,
                    intentTL, 0);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(mContext)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(name)
                            .setContentIntent(intent)
                            //.setOngoing(true)
                            .setContentText(text)
                            .setAutoCancel(true);

            Notification notification = builder.build();

            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(new Random().nextInt(), notification);
        }
    }

    public static boolean isInternetConnect(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        try {
            Snackbar.make(getApp().getWindow().getDecorView().getRootView(), "Нет подключения к интернету", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static class Status {
        private String status;
        private Date date;
        private String html;
        private TrackStatus success;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getHtml() {
            return html;
        }

        public void setHtml(String html) {
            this.html = html;
        }

        public TrackStatus getSuccess() {
            return success;
        }

        public void setSuccess(TrackStatus success) {
            this.success = success;
        }
    }

    public static class TaskObject {
        private String name;
        private String num;
        private String param;

        public TaskObject(String name, String num, String param) {
            this.name = name;
            this.num = num;
            this.param = param;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getParam() {
            return param;
        }

        public void setParam(String param) {
            this.param = param;
        }
    }

    public static class UpdateObject {
        private Integer countInitTracks = 0;
        private HashMap<String, UpdateTrackStatus> infoObject = new HashMap<>();
        private Integer countNewValues = 0;
        private Integer countErrorValues = 0;
        private boolean updateSuccess = false;

        public Integer getCountInitTracks() {
            return countInitTracks;
        }

        public void setCountInitTracks(Integer countInitTracks) {
            this.countInitTracks = countInitTracks;
        }

        public HashMap<String, UpdateTrackStatus> getInfoObject() {
            return infoObject;
        }

        public void setInfoObject(HashMap<String, UpdateTrackStatus> infoObject) {
            this.infoObject = infoObject;
        }

        public Integer getCountNewValues() {
            return countNewValues;
        }

        public void setCountNewValues(Integer countNewValues) {
            this.countNewValues = countNewValues;
        }

        public Integer getCountErrorValues() {
            return countErrorValues;
        }

        public void setCountErrorValues(Integer countErrorValues) {
            this.countErrorValues = countErrorValues;
        }

        public boolean isUpdateSuccess() {
            return updateSuccess;
        }

        public void setUpdateSuccess(boolean updateSuccess) {
            this.updateSuccess = updateSuccess;
        }

        public void recalcUpdate () {
            boolean result = true;
            countNewValues = 0;
            //если нет списка треков
            if (infoObject.size() == 0)
                result = false;
            for (Map.Entry<String, UpdateTrackStatus> mp : infoObject.entrySet()) {
                //если хоть один трек еще не обновился
                if (mp.getValue().equals(UpdateTrackStatus.NO_VALUE)) {
                    result = false;
                }
                //количество новых статусов
                if (mp.getValue().equals(UpdateTrackStatus.NEW_VALUE))
                    countNewValues++;

                //количество ошибок
                if (mp.getValue().equals(UpdateTrackStatus.ERROR))
                    countErrorValues++;
            }
            updateSuccess = result;
        }
    }

    //region checkPermission
    protected String[] checkPermission(){
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ){//Can add more as per requirement
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        return permissions.toArray(new String[0]);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recreate();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    //endregion
}
